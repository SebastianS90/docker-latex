FROM debian:latest

RUN apt-get update -qq -y \
 && apt-get install texlive-full openssh-client -y \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*
